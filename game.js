$(document).ready(function() {
  var xPos, yPos, step=1, enemyNo=0;
  var intervalIDboss, intervalIDlaser;
  var height = $(document).height();
  var width = $(document).width();
  var $sprite = $('#sprite');
  var $boss = $('#boss');
  $laser = $("#laser");
  var $starfield = $('#starfield');
  $healthBar = $('#healthbar');
  $boss.data("lives", 10);
  var playerHealth = 100;
  var enemyLives = 2;
  var noOfenemies=15;
  
  /*Initiate sounds */
  var shotSound = new buzz.sound("./sounds/shot", {
    formats: [ "wav" ],
    preload: true
    });
  var hitSound = new buzz.sound("./sounds/hit", {
    formats: [ "mp3" ],
    preload: true
    });
  var gameoverSound = new buzz.sound("./sounds/scream", {
    formats: [ "wav" ],
    preload: true
    });
  var wonSound = new buzz.sound("./sounds/won", {
    formats: [ "mp3" ],
    preload: true
    });
  var bgMusic = new buzz.sound("./sounds/bgmusic", {
    formats: [ "mp3" ],
    preload: true,
    autoplay: true,
    loop:true,
    volume: 10
    });  
  
  /*Initiate Enemy Starhsips */
  var enemyXPos = 20;
  while (noOfenemies >0) {
    enemyNo++
    noOfenemies--;
    enemyXPos += 100;
    $('<div class="enemies" id="enemy' + enemyNo  + '"></div>').insertAfter('#starfield');
    $enemy = $('#enemy' + enemyNo);
    $enemy.data("lives", enemyLives);
    $enemy.css('left', enemyXPos + 'px');
  }
  
  /* Run animations */
  animateBg($starfield);
  animateXEnemies($(".enemies"));
  //animateYEnemies($(".enemies"));
  animateBoss($boss);
  
  /* Collision detection loop */
  setInterval(function() {
   var $spriteBox = {
      x: parseInt($('#sprite').css("left")),
      y: parseInt($('#sprite').css("top")),
      width: $sprite.width() - 20,
      height: $sprite.height()  - 20
      }
    
    //check if ship and boss colide
    if ($boss !== null) {
      var $bossBox = {
      x: parseInt($('#boss').css("left")),
      y: parseInt($('#boss').css("top")),
      width: $boss.width() - 20,
      height: $sprite.height() - 20
      }
      checkCollision($spriteBox, $bossBox, function() { hitSound.play(); removePlayerHealth(); });
    }
 
    //check if ship and laser collide   
    if ($laser !== null) {
      var $spriteBox = {
      x: parseInt($('#sprite').css("left")),
      y: parseInt($('#sprite').css("top")),
      width: $sprite.width() - 20,
      height: $sprite.height()  - 20
      }
      var $laserBox = {
      x: parseInt($('#laser').css("left")),
      y: parseInt($('#laser').css("top")),
      width: $laser.width() - 10,
      height: $laser.height() - 10
      }
      if ($('#laser').is(':visible')) {  
        checkCollision($spriteBox, $laserBox, function() { hitSound.play(); removePlayerHealth(); });
      }
    }
    
    //check if ship and enemies colide
    if ( $(".enemies").each(function(){}).length > 0 ) {
    console.log("checkin")
      $(".enemies").each(function() {
        $enemy = $(this);
        var $enemyBox = {
        x: parseInt($(this).css("left")),
        y: parseInt($(this).css("top")),
        width: $(this).width(),
        height: $(this).height()
        }
        checkCollision($spriteBox, $enemyBox, function() { hitSound.play(); removePlayerHealth(); });
        });
      }
      
    //check if bullet and enemies colide
    var $bulletBox = {
      x: parseInt($("#bullet").css("left")),
      y: parseInt($("#bullet").css("top")),
      width: $("#bullet").width(),
      height: $("#bullet").height()
    }
    $(".enemies").each(function() {
      $enemy = $(this);
      var $enemyBox = {
      x: parseInt($(this).css("left")),
      y: parseInt($(this).css("top")),
      width: $(this).width(),
      height: $(this).height()
      }
      checkCollision($bulletBox, $enemyBox, function() {
        $("#bullet").remove();
        var currentLives;
        currentLives = $enemy.data("lives");
        $enemy.data("lives", currentLives - 1);
        if ($enemy.data("lives") == 0) { $enemy.remove(); $enemy = null; }
        });
      });
      
      //check if bullet and boss colide
      if ($boss !== null) {
        checkCollision($bulletBox, $bossBox, function() {
          $("#bullet").remove();
          var currentBossLives;
          currentBossLives = $boss.data("lives");
          $boss.data("lives", currentBossLives - 1);
          if ($boss.data("lives") == 0) {
            clearInterval(intervalIDboss);
            clearInterval(intervalIDlaser);
            $boss.remove();
            $laser.remove();
            $boss = null;
            $laser = null;
          }
        });
      }
      
      //check game status
      console.log(playerHealth);
      if (playerHealth < 0) {bgMusic.stop(); gameoverSound.play(); alert("I'm sorry but you LOSE! Click OK to play again."); window.location.href = "index.html"; }
      if ($boss == null && $(".enemies").each(function(){}).length == 0) { bgMusic.stop(); wonSound.play(); alert("You are awesome! You've WON! Click OK to play again."); window.location.href = "index.html"; };
      
  }, 20); //end of collision detection loop
  
  /*Spaceship movement events */
  $(document).mousemove(function(event) {
    xPos = event.pageX;
    yPos = event.pageY;
    if (xPos > 10 && xPos < (width - 140)) {
      window.requestAnimationFrame(function() { $sprite.css('left', xPos + "px")});
    }
    if (yPos > 10 && yPos < (height - 140)) {
      window.requestAnimationFrame(function() { $sprite.css('top', yPos + "px")});
    }
  });
  
  /* Bullet firing events */
  $(document).mousedown(function(event) {
      /*allow only 1 bullet on the screen at a time */
      if (parseInt($("#bullet").css('top')) > -10) { return; }

      shotSound.play();
      $('<div id="bullet"></div>').insertAfter('#starfield');
      $bullet = $('#bullet');
      bulletXPos = xPos+60; //offset the bullet to place it in the middle of the ship
      bulletYPos = yPos-15;
      $bullet.css('left', bulletXPos + 'px');
      $bullet.css('top', bulletYPos + 'px');
      animateBullet($bullet,bulletXPos ,bulletYPos);
    });
  
  /* Functions  */
  function checkCollision(rect1, rect2, successClosure) {
    if (rect1.x < rect2.x + rect2.width &&
      rect1.x + rect1.width > rect2.x &&
      rect1.y < rect2.y + rect2.height &&
      rect1.height + rect1.y > rect2.y) {
        successClosure();
    }
  } 
  
  function removePlayerHealth() {
    playerHealth = playerHealth - 10;
    $healthBar.css("width", playerHealth + "%");
    }
  
  function animateBullet($bullet, bulletXPos ,bulletYPos) {
    setInterval(function() {
    bulletYPos-=5;
      window.requestAnimationFrame(function() {
         $bullet.css('top', bulletYPos + 'px');
         if (parseInt($bullet.css('top')) < -10) {
         $bullet.remove();
         return;
         }
      });
    }, 1);
  }
  
  function animateXEnemies($enemies) {
  var StartYPosition = parseInt($enemies.css('top'));
   setInterval(function() {
      window.requestAnimationFrame(function() {
        $enemies.each(function() {
             $enemy = $(this);
             StartXPosition = parseInt($enemy.css('left'));
             if ($enemy.data("Xdirection") == "R" || $enemy.data("Xdirection") == null) { StartXPosition+=5};
             if ($enemy.data("Xdirection") == "L") { StartXPosition-=5};
             $enemy.css('left', StartXPosition + 'px');
             
             //change direction when going out of window
             StartXPosition = parseInt($enemy.css('left'));
             if (StartXPosition > width-45) { $enemy.data("Xdirection", "L") }
             if (StartXPosition < -10) { $enemy.data("Xdirection","R") }
        });
      });
    }, 1000/30);
  }
  
  function animateBoss($boss) {
  var StartXPosition = parseInt($boss.css('left'));
   intervalIDboss = setInterval(function() {
      window.requestAnimationFrame(function() {
        if ($boss.data("Xdirection") == "R" || $boss.data("Xdirection") == null) { StartXPosition+=5};
        if ($boss.data("Xdirection") == "L") { StartXPosition-=5};
        $boss.css('left', StartXPosition + 'px');
        //set left and right movement limits
        if (StartXPosition > width-300) { $boss.data("Xdirection", "L") }
        if (StartXPosition < 200) { $boss.data("Xdirection","R") }
      });
    }, 1000/30  );
    //animate Boss's laser cannon
    var i=0, curPos;
    intervalIDlaser = setInterval(function() {
      window.requestAnimationFrame(function() {
        curPos = parseInt($("#boss").css("left")) + 95;
        $laser.css("left", curPos + "px");
      });
    }, 10);
    //Turn the laser off/on
    setInterval(function() {
      $("#laser").toggle("slow");
    }, 5000);
  }
  
  function animateBg($starfield) {
    setInterval(function() {
      window.requestAnimationFrame(function() {
        $starfield.css('background-position', '0px ' + step++ + 'px');
      });
    }, 1000/30);
  }
  
});